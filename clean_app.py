import sys
import tempfile
import os
import zipfile
import shutil
import pathlib
import logging


def write_list_to_file(tmpdir, deleted_dirs):
    try:
        with open(tmpdir + "/cleaned.txt", "a") as f:
            for line in deleted_dirs:
                f.write(line + "\n")
    except Exception as exc:
        logging.error(f"File cleaned.txt writing error: {exc}")


def delete_dirs(path, check_file):

    deleted_dirs = []
    for dirpath, dirnames, files in os.walk(path):
        if dirpath == path:
            logging.info(f"Archive {dirpath} created successfully")
        else:
            has_file = False
            for file_name in files:
                if file_name == check_file:
                    has_file = True
            if not has_file:
                deleted_dirs.append(dirpath.replace(path + "/", ""))

                try:
                    shutil.rmtree(dirpath)
                except Exception as exc:
                    logging.error(f"Directory {dirpath} deleting error: {exc}")
                    return False

    deleted_dirs.sort()
    return deleted_dirs


def to_zip(path, filename):
    directory = pathlib.Path(path)
    try:
        with zipfile.ZipFile(filename, "w") as zf:
            for file_path in directory.rglob("*"):
                zf.write(file_path, arcname=file_path.relative_to(directory))
    except Exception as exc:
        logging.error(f"Archive creating error: {exc}")
        return False
    else:
        logging.info(f"Archive {filename} created successfully")
        return True


def main():
    archive_name = sys.argv[1]
    new_archive_name = archive_name.replace(".zip", "_new.zip")

    try:
        with tempfile.TemporaryDirectory() as tmpdir:
            try:
                with zipfile.ZipFile(archive_name, "r") as zf:
                    zf.extractall(path=tmpdir)
            except Exception as exc:
                logging.error(f"Archive file opening error: {exc}")
                return False
            else:
                logging.info(f"Archive file {archive_name} opened successfully")

                write_list_to_file(tmpdir, delete_dirs(tmpdir, "__init__.py"))
                to_zip(tmpdir, new_archive_name)

    except Exception as exc:
        logging.error(f"Temporary directory creating error: {exc}")
        return False
    else:
        logging.info(f"Temporary directory {tmpdir} created successfully")


if __name__ == "__main__":
    main()
